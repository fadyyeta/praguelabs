﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using praguelabs.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace praguelabs.Controllers
{
    public class InvoicesController : Controller
    {
        private pragueLabsEntities db = new pragueLabsEntities();
        //private string apiKey = "178bad9817b07e2063a45f5158a6eeff467733ff133fa5141322166416def40b";
        // GET: Invoices
        public ActionResult Index()
        {
            var invoices = db.Invoices.Include(i => i.store);
            return View(invoices.ToList());
        }
        public async Task<ActionResult> notPaid()
        {
            var client = new HttpClient();
            //Response.AddHeader("APIKey", apiKey);
            var response = await client.GetAsync("http://praguelabs.apphb.com/api/Payment");
            var invoices = await response.Content.ReadAsAsync<IEnumerable<Invoice>>();
            //var invoices = db.Invoices.Include(i => i.store);
            return View(invoices);
        }

        // GET: Invoices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // GET: Invoices/Create
        public ActionResult Create()
        {
            ViewBag.storeID = new SelectList(db.stores, "ID", "name");
            return View();
        }

        // POST: Invoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,invoiceNum,storeID,orderDate,paid")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                invoice.orderDate = DateTime.Now;
                db.Invoices.Add(invoice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.storeID = new SelectList(db.stores, "ID", "name", invoice.storeID);
            return View(invoice);
        }

        // GET: Invoices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            ViewBag.storeID = new SelectList(db.stores, "ID", "name", invoice.storeID);
            return View(invoice);
        }

        // POST: Invoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,invoiceNum,storeID,orderDate,paid")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.storeID = new SelectList(db.stores, "ID", "name", invoice.storeID);
            return View(invoice);
        }

        // GET: Invoices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            db.Invoices.Remove(invoice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
