﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using praguelabs.Models;
using Newtonsoft.Json.Linq;

namespace praguelabs.Controllers
{
    public class PaymentController : ApiController
    {
        private pragueLabsEntities db = new pragueLabsEntities();

        [HttpPatch]
        public IHttpActionResult PatchInvoice([FromUri] int ID, Invoice invoicey)
        {
            // path looks like >> http://localhost:5527/api/Payment/4 
            // data in request body looks like this
            //{
            //    "invoiceNum": "yeta patch",
            //	"paid" : "Yes",
            //	"orderDate" : "2019-04-08 12:59:46.000",
            //	"storeID" : 1
            //}
            var Invoice = db.Invoices.FirstOrDefault(c => c.ID == ID);
            if (Invoice == null)
                return NotFound();
            else
            {
                Invoice.invoiceNum = invoicey.invoiceNum;
                //Invoice.orderDate = invoicey.orderDate;
                //Invoice.storeID = invoicey.storeID;
                //Invoice.paid = invoicey.paid;
            }

            //db.Entry(Invoice).State = EntityState.Modified;


            db.SaveChanges();

            return Ok();
        }

        //// GET: api/Payment
        //public IQueryable<Invoice> GetInvoices()
        //{
        //    return db.Invoices;
        //}

        //// GET: api/Payment/5
        //[ResponseType(typeof(Invoice))]
        //public IHttpActionResult GetInvoice(int id)
        //{
        //    Invoice invoice = db.Invoices.Find(id);
        //    if (invoice == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(invoice);
        //}

        // PUT: api/Payment/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutInvoice(int id, Invoice invoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != invoice.ID)
            {
                return BadRequest();
            }

            db.Entry(invoice).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Payment
        [ResponseType(typeof(Invoice))]
        public IHttpActionResult PostInvoice(Invoice invoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Invoices.Add(invoice);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = invoice.ID }, invoice);
        }
        // POST: api/Payment
        [ResponseType(typeof(void))]
        public IHttpActionResult PayInvoice(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != invoice.ID)
            {
                return BadRequest();
            }

            //db.Entry(invoice).State = EntityState.Modified;
            db.payInv(id);
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);

        }


        // DELETE: api/Payment/5
        [ResponseType(typeof(Invoice))]
        public IHttpActionResult DeleteInvoice(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return NotFound();
            }

            db.Invoices.Remove(invoice);
            db.SaveChanges();

            return Ok(invoice);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InvoiceExists(int id)
        {
            return db.Invoices.Count(e => e.ID == id) > 0;
        }
        [HttpGet]
        public IEnumerable<Invoice> UnPaid()
        {
            //var invoices = db.Invoices.Include(i => i.store);
            var invoices = db.Invoices.Where(x => x.paid != "yes").ToList();

            return invoices;
        }
    }
}