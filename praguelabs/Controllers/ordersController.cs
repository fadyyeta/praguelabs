﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using praguelabs.Models;

namespace praguelabs.Controllers
{
    public class ordersController : Controller
    {
        private pragueLabsEntities db = new pragueLabsEntities();

        // GET: orders
        public ActionResult Index()
        {
            var orders = db.orders.Include(o => o.Invoice).Include(o => o.Item).Include(o => o.unit);
            return View(orders.ToList());
        }

        // GET: orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: orders/Create
        public ActionResult Create()
        {
            ViewBag.invoiceID = new SelectList(db.Invoices, "ID", "invoiceNum");
            ViewBag.itemID = new SelectList(db.Items, "ID", "name");
            ViewBag.unitID = new SelectList(db.units, "ID", "name");
            return View();
        }

        // POST: orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,itemID,unitID,invoiceID,Qty,discount")] order order)
        {
            if (ModelState.IsValid)
            {
                db.orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.invoiceID = new SelectList(db.Invoices, "ID", "invoiceNum", order.invoiceID);
            ViewBag.itemID = new SelectList(db.Items, "ID", "name", order.itemID);
            ViewBag.unitID = new SelectList(db.units, "ID", "name", order.unitID);
            return View(order);
        }

        // GET: orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.invoiceID = new SelectList(db.Invoices, "ID", "invoiceNum", order.invoiceID);
            ViewBag.itemID = new SelectList(db.Items, "ID", "name", order.itemID);
            ViewBag.unitID = new SelectList(db.units, "ID", "name", order.unitID);
            return View(order);
        }

        // POST: orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,itemID,unitID,invoiceID,Qty,discount")] order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.invoiceID = new SelectList(db.Invoices, "ID", "invoiceNum", order.invoiceID);
            ViewBag.itemID = new SelectList(db.Items, "ID", "name", order.itemID);
            ViewBag.unitID = new SelectList(db.units, "ID", "name", order.unitID);
            return View(order);
        }

        // GET: orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            order order = db.orders.Find(id);
            db.orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
