﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace praguelabs.MessageHandlers
{
    public class APIKeyMessageHandler : DelegatingHandler
    {
        // PragueLabs in  "sha256" is "178bad9817b07e2063a45f5158a6eeff467733ff133fa5141322166416def40b",

        private const string APIKeyToCheck = "178bad9817b07e2063a45f5158a6eeff467733ff133fa5141322166416def40b";
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage, CancellationToken cancellationToken)
        {
            bool validKey = false;
            IEnumerable<string> requestHeaders;
            var checkApiKeyExists = httpRequestMessage.Headers.TryGetValues("APIKey", out requestHeaders);
            if (checkApiKeyExists)
            {
                if (requestHeaders.FirstOrDefault().Equals(APIKeyToCheck))
                {
                    validKey = true;
                }

            }
            if (!validKey)
            {
                return httpRequestMessage.CreateResponse(HttpStatusCode.Forbidden, "Invalid API Key");

            }
            var response = await base.SendAsync(httpRequestMessage, cancellationToken);
            return response;
        }


    }
}