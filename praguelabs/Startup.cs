﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(praguelabs.Startup))]
namespace praguelabs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
