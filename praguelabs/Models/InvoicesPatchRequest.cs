﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace praguelabs.Models
{
    public class InvoicesPatchRequest
    {
        public InvoicesPatchRequest()
        {
            this.orders = new HashSet<order>();
        }

        public int ID { get; set; }
        public string invoiceNum { get; set; }
        public Nullable<int> storeID { get; set; }
        public Nullable<System.DateTime> orderDate { get; set; }
        public string paid { get; set; }

        public virtual store store { get; set; }
        public virtual ICollection<order> orders { get; set; }
    }
}